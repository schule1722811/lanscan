import psutil, time, os, subprocess

def user_choose_network(network_list):
    print("The following connected networks have been detected.")
    for network in network_list:
        print(f"ID: {network['id']} Name: {network['name']} Addr: {network['address']} Mask: {network['netmask']}")
    print("Select the ID of one of the above networks to scan.")
    while True:
        try:
            chosen_network = int(input()) 
        except Exception:
            print("Not a valid integer, enter a valid integer!")
            continue
        if int(chosen_network) not in range(len(network_list)):
            print("Not a valid network ID, enter a valid network ID!")
            continue
        else: break
    return network_list[chosen_network]

    

def get_connected_networks():
    network_list = []
    net_id = 0
    connections = psutil.net_if_addrs()
    for network in connections:
        for value in connections[network]:
            if value.family == 2:
                network_dict = {
                    'id' : net_id,
                    'name' : network,
                    'address' : value.address,
                    'netmask' : value.netmask
                }
                network_list.append(network_dict)
                net_id += 1
    return network_list

def printer(lan_list):
    output = (
'''============================================================================
|| IP              || MAC               || Hostname             || Ping   ||
''')
    for entry in lan_list:
        output += '|| ' + norm(entry['IP'], 15) + ' || ' + norm(entry['mac'], 17) + ' || ' + norm(entry['hostname'], 20) + ' || ' + norm(entry['ping'], 6) + ' ||\n'
    print(output)

def norm(string, length):
    return string + ' ' * (length-len(string))
