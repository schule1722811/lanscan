import threading, subprocess, re

class Network:
    def __init__(self, addr, mask):
        self.addr = list(map(int, addr.split('.')))
        self.mask = list(map(int, mask.split('.')))
        self.net_addr = self.get_net_addr()
        self.broad_addr = self.get_broad_addr()
        self.addr_list = self.get_addr_list()
        self.net_data = []

    def get_net_addr(self):
        net_addr = []
        for i in range(len(self.addr)):
            net_addr.append(self.addr[i] & self.mask[i])
        return net_addr
    
    def get_broad_addr(self):
        broad_addr = []
        for i in range(len(self.mask)):
            broad_addr.append(self.net_addr[i] | (255 - self.mask[i]))
        return broad_addr
    
    def get_addr_list(self):
        addr_list = []
        counter = self.net_addr[:]
        while True:
            if counter[3] == 255:
                counter[3] = 0
                counter[2] += 1
            elif counter[2] == 255:
                counter[2] = 0
                counter[1] += 1
            elif counter[1] == 255:
                counter[1] = 0
                counter[0] += 1
            else:
                counter[3] += 1
            if counter != self.broad_addr:
                addr_list.append([counter[0], counter[1], counter[2], counter[3]])
            else:
                break
        return addr_list

    def single_thread_scan(self, start, end):
        for addr in self.addr_list[start:end]:
            dst = str(addr[0])+'.'+str(addr[1])+'.'+str(addr[2])+'.'+str(addr[3])
            hostname = str(self.get_hostname(dst))
            ping = str(self.get_ping(dst))
            mac = str(self.get_mac(dst))
            if ping != 'None' or hostname != 'None':
                entry = {'IP': dst, 'mac': mac, 'hostname': hostname, 'ping': ping}
                self.net_data.append(entry)

    def scanner(self, no_threads):
        if no_threads > len(self.addr_list):
            no_threads = len(self.addr_list)                                     
        split_size = len(self.addr_list) // no_threads
        threads = []
        for i in range(no_threads):
            # determine the indices of the list this thread will handle
            start = i * split_size
            # special case on the last chunk to account for uneven splits
            end = None if i+1 == no_threads else (i+1) * split_size
            # create the thread
            threads.append(
                threading.Thread(target=self.single_thread_scan, args=(start, end)))
            threads[-1].start() # start the thread we just created

        # wait for all threads to finish    
        for t in threads:
            t.join()
            
    def get_hostname(self, dst):
        proc = subprocess.run(
            ['nslookup', dst],
            stdout = subprocess.PIPE,
            stderr = subprocess.DEVNULL)
        output = str(proc.stdout)
        if 'Name: ' in output:
            dev_name = output.split('Name:')[1].split('Address: ')[0][:-4].strip()
            return dev_name

    def get_ping(self, dst):
        proc = subprocess.run(
            ['ping', dst],
            stdout = subprocess.PIPE,
            stderr = subprocess.DEVNULL)
        output = str(proc.stdout)
        if 'Ca. Zeitangaben in Millisek.:' in output:
            ping = output.split('Mittelwert =')[1][:-5]
            return ping

    def get_mac(self, dst):
        proc = subprocess.run(
            ['ARP', '-a', dst],
            stdout = subprocess.PIPE,
            stderr = subprocess.DEVNULL)
        output = str(proc.stdout)
        regex = re.compile("([0-9A-Fa-f]{2}[-:]){5}([0-9A-Fa-f]{2})")
        try: 
            mac = regex.search(output).group(0)
            return mac
        except: 
            return