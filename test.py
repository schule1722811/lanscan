import unittest
#import lanscan
from network import Network

class test_lanscan(unittest.TestCase):

    def setUp(self):
        self.net = Network("192.168.178.5", "255.255.255.252")

    # Unit test: Is the address string split into a list correctly?
    def test_addr(self):
        self.assertEqual(self.net.addr, [192,168,178,5])

    # Unit test: Is the Subnet Mask split into a list correctly?
    def test_mask(self):
        self.assertEqual(self.net.mask, [255,255,255,252])

    # Intergration test: Is the network address calculated correctly?
    # The function net.net_addr relies on the functions net.net_mask and net.net_addr
    def test_net_addr(self):
        self.assertEqual(self.net.net_addr, [192,168,178,4])

    # Intergration test: Is the broadcast address calculated correctly?
    # The function net.broadcast_addr relies on the functions net.net_mask and net.net_addr
    def test_broadcast_addr(self):
        self.assertEqual(self.net.broad_addr, [192,168,178,7])

    # Intergration test: Is the list of addresses calculated correctly?
    # The function net.addr_list relies on the functions net.broad_addr and net.net_addr
    def test_network_list(self):
        self.assertEqual(self.net.addr_list, [[192,168,178,5],[192,168,178,6]])

if __name__ == '__main__':
    unittest.main()