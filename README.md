# LANScan

Dieses Python-Programm ermöglicht es, ein Netzwerk zu scannen, um Informationen über die verbundenen Geräte zu sammeln.


## Installationsanleitung

Um das Programm auszuführen, stellen Sie sicher, dass Sie Python installiert haben. Zusätzlich müssen sie pip install -r requirements.txt installieren und python main.py ausführen.


## Erreichte Ziele und ToDo's

Erreichte Ziele:

Das Programm kann ein Netzwerk scannen und Informationen über die verbundenen Geräte sammeln.
Es unterstützt die gleichzeitige Ausführung von Scans mit mehreren Threads.

Offene ToDo's:

Implementierung einer Benutzeroberfläche für eine benutzerfreundlichere Interaktion.
Verbesserung der Fehlerbehandlung und Robustheit des Programms.

## Problemlösung

Ein Problem, das während der Entwicklung auftrat, war die Zuverlässigkeit der Geräteerkennung. Insbesondere das Abrufen von Hostnamen und MAC-Adressen war manchmal unzuverlässig aufgrund von Netzwerkverzögerungen und unvollständigen Antworten. Um dieses Problem zu lösen, wurde die Verwendung von regulären Ausdrücken für die Extraktion von MAC-Adressen implementiert, und die Fehlerbehandlung wurde verbessert, um unerwartete Ausgaben angemessen zu behandeln.

## Zukünftige Erweiterungsmöglichkeiten

Benutzeroberfläche: Implementierung einer GUI für eine intuitivere Nutzung des Programms durch Benutzer mit weniger technischem Hintergrund.

Erweiterte Netzwerkanalyse: Integration von Funktionen zur Analyse von Netzwerkverkehr und Sicherheitsprüfungen für eine umfassendere Netzwerkerfahrung.


## Teamarbeit

Software @all:

Gemeinsam arbeiteten wir an einem Laptop, um die Software für einen internen Netzwerkscanner zu entwickeln. Auf diesem Laptop haben wir virtuelle Clients konfiguriert, die in einem entsprechenden virtuellen Netzwerk eingebunden waren. Mittels dieser virtuellen Clients führten wir umfassende Tests durch, darunter auch das Scannen des Netzwerks, um die Leistung unserer Software zu überprüfen.

README @Manav:

Im Verlauf des Projekts wurde die README kontinuierlich weiterentwickelt, wobei besonderes Augenmerk darauf lag, sie für externe Betrachter verständlich und zugänglich zu gestalten. Durch eine klare Strukturierung und präzise Formulierung wird sichergestellt, dass potenzielle Benutzer einen umfassenden Einblick in die Funktionalität und Verwendung des Programms erhalten.

Softwareerweiterung @Jaime: 

Nachdem wir die Software-Programmierung abgeschlossen hatten, konzentrierten wir uns darauf, eine grafische Benutzeroberfläche (GUI) für das Programm zu entwickeln. Bedauerlicherweise waren wir zum Zeitpunkt der Abgabe noch nicht damit fertig geworden. Trotzdem haben wir beschlossen, die GUI auf Git hochzuladen, damit der aktuelle Entwicklungsstand bewertet werden kann.

Sequenzdiagramm/pap @Louis:

Gemeinsam haben wir ein Sequenzdiagramm erstellt, um den Nachrichtenablauf in GitLab zu veranschaulichen, sowie ein PAP (Programmablaufplan), um unser Programm in seine einzelnen Schritte zu gliedern. Diese Methoden unterstützten uns dabei, eine klare Struktur und Verständnis für das Projekt zu schaffen, während wir uns auf die Entwicklung konzentrierten.

Softwareentwicklung und Pipeline @Henry:

Wir haben unser Programm aus dem letzten Block um neue Funktionen erweitert (automatische Netzwerkerkennung) und den Code im Rahmen des objektorientierten Programmierparadigmas (OOP) neu geschrieben. Um Fehler während dieses Prozesses zu vermeiden, haben wir automatisierte Unit- und Integrationstests mithilfe der GitLab Runners erstellt und angewendet.



## Sequenzdiagramm

![](Sequenzdiagramm.png)

## Programm Ablauf Plan

![](pap.png)