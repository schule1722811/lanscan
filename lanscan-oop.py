import threading

from network import Network
import utilities

def main():
    connected_networks = utilities.get_connected_networks()
    to_scan = utilities.user_choose_network(connected_networks)
    net = Network(to_scan['address'], to_scan['netmask'])
    no_threads = 1000
    scanning = threading.Thread(target=net.scanner, args=(no_threads,))
    scanning.start()
    scanning.join()
    utilities.printer(net.net_data)

if __name__ == "__main__":
    main()